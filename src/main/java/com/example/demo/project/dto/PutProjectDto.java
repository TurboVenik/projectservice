package com.example.demo.project.dto;

import lombok.Data;

@Data
public class PutProjectDto {

    private String name;
    private String description;
    private String requirements;
}
