package com.example.demo.project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProjectResponseDto {

    private Integer id;
    private String name;
    private String description;
    private String requirements;
}
