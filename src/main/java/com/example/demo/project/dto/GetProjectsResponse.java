package com.example.demo.project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GetProjectsResponse {

    private List<ProjectResponseDto> projects;
}
