package com.example.demo.project.dto;

import lombok.Data;

@Data
public class PostProjectRequest {

    private String name;
    private String description;
    private String requirements;
}
