package com.example.demo.project.controller;

import com.example.demo.base.controller.BaseApiController;
import com.example.demo.base.model.ResponseModel;
import com.example.demo.project.dto.GetProjectsResponse;
import com.example.demo.project.dto.PostProjectRequest;
import com.example.demo.project.dto.ProjectResponseDto;
import com.example.demo.project.dto.PutProjectDto;
import com.example.demo.project.entity.ProjectEntity;
import com.example.demo.project.repository.ProjectRepository;
import com.example.demo.project.utils.ProjectMapper;
import com.example.demo.task.entity.TaskEntity;
import com.example.demo.task.enums.TaskStatusEnum;
import com.example.demo.task.repository.TaskRepository;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
public class ProjectController implements BaseApiController {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    private final ProjectMapper projectMapper;

    public ProjectController(ProjectRepository projectRepository, ProjectMapper projectMapper, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.projectMapper = projectMapper;
    }

    private List<ProjectEntity> getMockProjects() {

        return Arrays.asList(
                new ProjectEntity("Проект 1", "Описание проекта 1", "сделать;чтобы все были довольны;завтра;"),
                new ProjectEntity("Проект 2", "Описание проекта 2", "хоть как то;вчера;")
        );
    }

    public List<TaskEntity> getMockTasks() {

        return Arrays.asList(
                new TaskEntity("Задача 1", "Придумать задачу 2", TaskStatusEnum.CLOSED.toString(), 1, "u1", "u2", new Date(), new Date()),
                new TaskEntity("Задача 2", "Придумать задачу 3", TaskStatusEnum.IN_PROGRESS.toString(), 1, "u1", "u2", new Date(), null),
                new TaskEntity("Задача 3", "Придумать задачу 4", TaskStatusEnum.NEW.toString(), 1, "u1", null, new Date(), null),

                new TaskEntity("Задача 1", "Сделать проект", TaskStatusEnum.IN_PROGRESS.toString(), 2, "u1", "u2", new Date(), null)
        );
    }

    @PostConstruct
    public void fill() {
        projectRepository.deleteAll();
        taskRepository.deleteAll();
        projectRepository.saveAll(getMockProjects());
        taskRepository.saveAll(getMockTasks());
    }

    public static void writeProjects(PrintWriter writer, List<ProjectResponseDto> projects) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {


        ColumnPositionMappingStrategy<ProjectResponseDto> mapStrategy
                = new ColumnPositionMappingStrategy<>();

        mapStrategy.setType(ProjectResponseDto.class);

        String[] columns = new String[]{"id", "name", "description", "requirements"};
        mapStrategy.setColumnMapping(columns);

        StatefulBeanToCsv<ProjectResponseDto> btcsv = new StatefulBeanToCsvBuilder<ProjectResponseDto>(writer)
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withMappingStrategy(mapStrategy)
                .withSeparator(',')
                .build();

        btcsv.write(projects);
    }

    @RequestMapping(value = "/projects/csv", produces = "text/csv")
    public void projectsCsv(HttpServletResponse response) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        List<ProjectResponseDto> dtos = projectMapper.projectsToDto(projectRepository.findAll());
        response.setHeader("Content-Disposition", "attachment; filename=projects.csv");
        writeProjects(response.getWriter(), dtos);
    }

    @RequestMapping(value = "/projects", method = GET)
    @ResponseBody
    public ResponseModel<GetProjectsResponse> get() {
        System.out.println("...::: get Projects :::...");

        GetProjectsResponse responseDto = new GetProjectsResponse(projectMapper.projectsToDto(projectRepository.findAll()));

        System.out.println(responseDto);

        return ResponseModel.createSuccess(responseDto);
    }

    @RequestMapping(value = "/projects/{projectId}", method = GET)
    public ResponseModel<ProjectResponseDto> getById(@PathVariable Integer projectId) {
        System.out.println("...::: get Projects ById :::...");
        System.out.println(projectId);

        Optional<ProjectEntity> byId = projectRepository.findById(Long.valueOf(projectId));

        return ResponseModel.createSuccess(byId.map(projectMapper::projectToDto).orElse(null));
    }

    @RequestMapping(value = "/projects", method = POST)
    public ResponseModel<ProjectResponseDto> post(@RequestBody PostProjectRequest projectDto) {
        System.out.println("...::: post Projects :::...");
        System.out.println(projectDto);

        ProjectEntity entity = new ProjectEntity();
        entity.setName(projectDto.getName());
        entity.setDescription(projectDto.getDescription());
        entity.setRequirements(projectDto.getRequirements());
        projectRepository.save(entity);

        System.out.println(entity);

        return ResponseModel.createSuccess(projectMapper.projectToDto(entity));
    }

    @RequestMapping(value = "/projects/{projectId}", method = PUT)
    public ResponseModel<ResponseEntity> put(@PathVariable Integer projectId, @RequestBody PutProjectDto projectDto) {
        System.out.println("...::: put Projects :::...");
        System.out.println(projectId);
        System.out.println(projectDto);

        Optional<ProjectEntity> byId = projectRepository.findById(Long.valueOf(projectId));

        if (!byId.isPresent()) {
            return null;
        }

        ProjectEntity entity = byId.get();

        entity.setName(projectDto.getName());
        entity.setDescription(projectDto.getDescription());
        entity.setRequirements(projectDto.getRequirements());

        projectRepository.save(entity);

        return ResponseModel.createSuccess(new ResponseEntity(HttpStatus.NO_CONTENT));
    }

    @RequestMapping(value = "/projects/{projectId}", method = DELETE)
    public ResponseModel<ResponseEntity> delete(@PathVariable Integer projectId) {
        System.out.println("...::: delete Projects :::...");
        System.out.println(projectId);

        projectRepository.deleteById(Long.valueOf(projectId));

        return ResponseModel.createSuccess(new ResponseEntity(HttpStatus.NO_CONTENT));
    }
}
