package com.example.demo.project.repository;

import com.example.demo.project.entity.ProjectEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProjectRepository extends CrudRepository<ProjectEntity, Long> {
}

