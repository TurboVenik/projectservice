package com.example.demo.project.utils;

import com.example.demo.project.dto.ProjectResponseDto;
import com.example.demo.project.entity.ProjectEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectMapper {

    public List<ProjectResponseDto> projectsToDto(Iterable<ProjectEntity> entities) {
        ArrayList<ProjectResponseDto> list = new ArrayList<>();
        for (ProjectEntity entity : entities) {
            list.add(projectToDto(entity));
        }
        return list;
    }

    public ProjectResponseDto projectToDto(ProjectEntity entity) {
        return new ProjectResponseDto((int) entity.getId(), entity.getName(), entity.getDescription(), entity.getRequirements());
    }
}
