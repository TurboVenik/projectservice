package com.example.demo.task.enums;

public enum TaskStatusEnum {
    NEW,
    IN_PROGRESS,
    CLOSED
}
