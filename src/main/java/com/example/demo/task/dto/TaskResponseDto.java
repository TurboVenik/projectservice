package com.example.demo.task.dto;

import com.example.demo.task.enums.TaskStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class TaskResponseDto {

    private Integer id;

    private String name;
    private String description;
    private TaskStatusEnum status;
    private Integer projectId;
    private String createrId;
    private String executorId;

    private Date startTime;
    private Date endTime;
}
