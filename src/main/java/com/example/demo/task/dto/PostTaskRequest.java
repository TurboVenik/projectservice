package com.example.demo.task.dto;

import lombok.Data;

@Data
public class PostTaskRequest {

    private String createrId;
    private String executorId;
    private String name;
    private String description;
}
