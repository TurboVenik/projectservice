package com.example.demo.task.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GetTasksResponse {

    private List<TaskResponseDto> tasks;
}
