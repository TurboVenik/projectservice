package com.example.demo.task.dto;

import com.example.demo.task.enums.TaskStatusEnum;
import lombok.Data;


@Data
public class PutTaskDto {

    private String name;
    private String description;
    private TaskStatusEnum status;
    private String createrId;
    private String executorId;
}
