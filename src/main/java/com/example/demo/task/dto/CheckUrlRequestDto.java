package com.example.demo.task.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CheckUrlRequestDto {

    private String login;
    private Integer projectId;
}
