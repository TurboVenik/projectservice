package com.example.demo.task.repository;


import com.example.demo.task.entity.TaskEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends CrudRepository<TaskEntity, Long> {
    List<TaskEntity> findByProjectId(int projectId);
    Optional<TaskEntity> findByProjectIdAndId(int projectId, long id);
}

