package com.example.demo.task.utils;


import com.example.demo.task.dto.TaskResponseDto;
import com.example.demo.task.entity.TaskEntity;
import com.example.demo.task.enums.TaskStatusEnum;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TaskMapper {

    public List<TaskResponseDto> tasksToDto(Iterable<TaskEntity> entities) {
        ArrayList<TaskResponseDto> list = new ArrayList<>();
        for (TaskEntity entity : entities) {
            list.add(taskToDto(entity));
        }
        return list;
    }

    public TaskResponseDto taskToDto(TaskEntity entity) {
        return new TaskResponseDto((int) entity.getId(), entity.getName(), entity.getDescription(),
                TaskStatusEnum.valueOf(entity.getStatus()), entity.getProjectId(), entity.getCreaterId(), entity.getExecutorId(), entity.getStartTime(), entity.getEndTime());
    }
}
