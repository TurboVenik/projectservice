package com.example.demo.task.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    private String name;
    @Length(max = 1000)
    private String description;
    private String status;
    private int projectId;
    private String createrId;
    private String executorId;

    private Date startTime;
    private Date endTime;


    public TaskEntity(String name, @Length(max = 1000) String description, String status, int projectId, String createrId, String executorId, Date startTime, Date endTime) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.projectId = projectId;
        this.createrId = createrId;
        this.executorId = executorId;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}



