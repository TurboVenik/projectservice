package com.example.demo.task.controller;

import com.example.demo.base.controller.BaseApiController;
import com.example.demo.base.model.ResponseModel;
import com.example.demo.project.dto.ProjectResponseDto;
import com.example.demo.task.dto.*;
import com.example.demo.task.entity.TaskEntity;
import com.example.demo.task.enums.TaskStatusEnum;
import com.example.demo.task.repository.TaskRepository;
import com.example.demo.task.utils.TaskMapper;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.*;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

@RestController
public class TaskController implements BaseApiController {

    private final TaskRepository taskRepository;

    private final TaskMapper taskMapper;

    private RestTemplate client = new RestTemplate();


    @Value("${auth.checkurl}")
    private String checkURL;


    public TaskController(TaskRepository taskRepository, TaskMapper taskMapper) {
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
    }

    private boolean checkURL(String login, Integer projectId) {

        ResponseEntity<CheckUrlResponseDto> response = client.postForEntity(checkURL, new CheckUrlRequestDto(login, projectId), CheckUrlResponseDto.class);
        return response.getBody() != null &&
                (response.getBody().getRole().equals("admin") ||
                        response.getBody().getRole().equals("manager") ||
                        response.getBody().getRole().equals("executor"));
    }

    public static void writeTasks(PrintWriter writer, List<TaskResponseDto> tasks) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {


        ColumnPositionMappingStrategy<TaskResponseDto> mapStrategy
                = new ColumnPositionMappingStrategy<>();

        mapStrategy.setType(TaskResponseDto.class);
        String[] columns = new String[]{"id", "name", "description", "status", "projectId", "createrId", "executorId"};
        mapStrategy.setColumnMapping(columns);

        StatefulBeanToCsv<TaskResponseDto> btcsv = new StatefulBeanToCsvBuilder<TaskResponseDto>(writer)
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withMappingStrategy(mapStrategy)
                .withSeparator(',')
                .build();

        btcsv.write(tasks);
    }

    @RequestMapping(value = "/projects/{projectId}/tasks/csv", produces = "text/csv")
    public void projectsCsv(HttpServletResponse response, @PathVariable Integer projectId) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Iterable<TaskEntity> all = taskRepository.findByProjectId(projectId);

        List<TaskResponseDto> responseDtos = taskMapper.tasksToDto(all);
        response.setHeader("Content-Disposition", "attachment; filename=tasks.csv");
        writeTasks(response.getWriter(), responseDtos);
    }


    @RequestMapping(value = "/projects/{projectId}/tasks", method = GET)
    public ResponseModel<GetTasksResponse> get(@PathVariable Integer projectId) {
        System.out.println("...::: get Tasks :::...");
        System.out.println(projectId);

        Iterable<TaskEntity> all = taskRepository.findByProjectId(projectId);

        List<TaskResponseDto> responseDtos = taskMapper.tasksToDto(all);

        GetTasksResponse responseDto = new GetTasksResponse(responseDtos);

        System.out.println(responseDto);
        return ResponseModel.createSuccess(responseDto);
    }


    @RequestMapping(value = "/projects/{projectId}/tasks/{taskId}", method = GET)
    public ResponseModel<TaskResponseDto> getById(@PathVariable Integer projectId, @PathVariable Integer taskId) {
        System.out.println("...::: get Tasks ById :::...");
        System.out.println(projectId);
        System.out.println(taskId);

        Optional<TaskEntity> byProjectIdAndId = taskRepository.findByProjectIdAndId(projectId, taskId);
        return byProjectIdAndId.map(taskEntity -> ResponseModel.createSuccess(taskMapper.taskToDto(taskEntity))).orElse(null);
    }


    @RequestMapping(value = "/projects/{projectId}/tasks", method = POST)
    public ResponseModel<TaskResponseDto> post(@PathVariable Integer projectId, @RequestBody PostTaskRequest taskDto) {
        System.out.println("...::: post Tasks :::...");
        System.out.println(projectId);
        System.out.println(taskDto);

        if (!checkURL(taskDto.getExecutorId(), projectId)) {
            return ResponseModel.createError(String.format("User %s not in project %s", taskDto.getExecutorId(), projectId));
        }

        TaskEntity entity = new TaskEntity();
        entity.setName(taskDto.getName());
        entity.setDescription(taskDto.getDescription());
        entity.setStatus(String.valueOf(TaskStatusEnum.NEW));
        entity.setProjectId(projectId);
        entity.setCreaterId(taskDto.getCreaterId());
        entity.setExecutorId(taskDto.getExecutorId());
        entity.setStartTime(new Date());

        taskRepository.save(entity);

        System.out.println(entity);

        return ResponseModel.createSuccess(taskMapper.taskToDto(entity));
    }

    @RequestMapping(value = "/projects/{projectId}/tasks/{taskId}", method = PUT)
    public ResponseModel<ResponseEntity> put(@PathVariable Integer projectId, @PathVariable Integer taskId, @RequestBody PutTaskDto projectDto) {
        System.out.println("...::: put Tasks :::...");
        System.out.println(projectId);
        System.out.println(taskId);
        System.out.println(projectDto);

        if (!checkURL(projectDto.getExecutorId(), projectId)) {
            return ResponseModel.createError(String.format("User %s not in project %s", projectDto.getExecutorId(), projectId));
        }

        Optional<TaskEntity> byProjectIdAndId = taskRepository.findByProjectIdAndId(projectId, taskId);

        if (!byProjectIdAndId.isPresent()) {
            return null;
        }

        TaskEntity taskEntity = byProjectIdAndId.get();

        taskEntity.setStatus(projectDto.getStatus().toString());
        taskEntity.setDescription(projectDto.getDescription());
        taskEntity.setName(projectDto.getName());
        taskEntity.setCreaterId(projectDto.getCreaterId());
        taskEntity.setExecutorId(projectDto.getExecutorId());

        if (projectDto.getStatus() == TaskStatusEnum.CLOSED) {
            taskEntity.setEndTime(new Date());
        }
        taskRepository.save(taskEntity);

        return ResponseModel.createSuccess(new ResponseEntity(HttpStatus.NO_CONTENT));
    }

    @RequestMapping(value = "/projects/{projectId}/tasks/{taskId}", method = DELETE)
    public ResponseModel<ResponseEntity> delete(@PathVariable Integer projectId, @PathVariable Integer taskId) {
        System.out.println("...::: delete Tasks :::...");
        System.out.println(projectId);
        System.out.println(taskId);

        taskRepository.deleteById(Long.valueOf(taskId));

        return ResponseModel.createSuccess(new ResponseEntity(HttpStatus.NO_CONTENT));
    }
}
