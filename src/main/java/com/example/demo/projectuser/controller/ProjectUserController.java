package com.example.demo.projectuser.controller;

import com.example.demo.base.controller.BaseApiController;
import com.example.demo.base.model.ResponseModel;
import com.example.demo.project.dto.GetProjectsResponse;
import com.example.demo.project.dto.PostProjectRequest;
import com.example.demo.project.dto.ProjectResponseDto;
import com.example.demo.project.dto.PutProjectDto;
import com.example.demo.projectuser.dto.GetProjectUsersResponse;
import com.example.demo.projectuser.dto.ProjectUserDto;
import com.example.demo.projectuser.dto.PutProjectUserDto;
import com.example.demo.task.dto.GetTasksResponse;
import com.example.demo.task.dto.PostTaskRequest;
import com.example.demo.task.dto.PutTaskDto;
import com.example.demo.task.dto.TaskResponseDto;
import com.example.demo.task.enums.TaskStatusEnum;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

import static org.springframework.web.bind.annotation.RequestMethod.*;

//@RestController
public class ProjectUserController implements BaseApiController {


    @RequestMapping(value = "/users", method = GET)
    public ResponseModel<GetProjectUsersResponse> get() {
        System.out.println("...::: get users :::...");

        ArrayList<ProjectUserDto> dtos = new ArrayList<>();

        dtos.add(new ProjectUserDto(1));
        dtos.add(new ProjectUserDto(2));
        dtos.add(new ProjectUserDto(3));

        GetProjectUsersResponse responseDto = new GetProjectUsersResponse(dtos);

        System.out.println(responseDto);
        return ResponseModel.createSuccess(responseDto);
    }


    @RequestMapping(value = "/users/{userId}", method = GET)
    public ResponseModel<ProjectUserDto> getById(@PathVariable Integer userId) {
        System.out.println("...::: get Users ById :::...");
        System.out.println(userId);

        ProjectUserDto responseDto = new ProjectUserDto(userId);

        System.out.println(responseDto);
        return ResponseModel.createSuccess(responseDto);
    }


    @RequestMapping(value = "/users/{userId}", method = PUT)
    public ResponseModel<ResponseEntity> put(@PathVariable Integer userId, @RequestBody PutProjectUserDto projectDto) {
        System.out.println("...::: put Projects :::...");
        System.out.println(userId);
        System.out.println(projectDto);

        return ResponseModel.createSuccess(new ResponseEntity(HttpStatus.NO_CONTENT));
    }
}
