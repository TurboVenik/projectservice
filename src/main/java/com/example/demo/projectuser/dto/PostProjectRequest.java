package com.example.demo.projectuser.dto;

import lombok.Data;

@Data
public class PostProjectRequest {

    private String name;
    private String description;
}
