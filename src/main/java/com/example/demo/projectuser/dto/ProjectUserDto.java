package com.example.demo.projectuser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProjectUserDto {

    private Integer id;
}
