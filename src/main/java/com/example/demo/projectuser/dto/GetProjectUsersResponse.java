package com.example.demo.projectuser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GetProjectUsersResponse {

    private List<ProjectUserDto> users;
}
