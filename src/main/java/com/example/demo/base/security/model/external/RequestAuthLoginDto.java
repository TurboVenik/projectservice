package com.example.demo.base.security.model.external;

public class RequestAuthLoginDto {

    private String login;
    private String projectId;

    public RequestAuthLoginDto() {
    }

    public RequestAuthLoginDto(String login, String projectId) {
        this.login = login;
        this.projectId = projectId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
