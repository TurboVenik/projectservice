package com.example.demo.base.security.model.internal;

public class ConstantRoles {

    public static String ADMIN = "admin";

    public static String MANAGER = "manager";

    public static String DEVELOPER = "executor";
}
