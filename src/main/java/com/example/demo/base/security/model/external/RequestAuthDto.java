package com.example.demo.base.security.model.external;

public class RequestAuthDto {

    private String cookie;
    private String projectId;

    public RequestAuthDto() {
    }

    public RequestAuthDto(String cookie, String projectId) {
        this.cookie = cookie;
        this.projectId = projectId;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
