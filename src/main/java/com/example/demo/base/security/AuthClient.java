package com.example.demo.base.security;

import com.example.demo.base.security.model.external.RequestAuthDto;
import com.example.demo.base.security.model.external.RequestAuthLoginDto;
import com.example.demo.base.security.model.external.ResponseAuthDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AuthClient {


    @Value("${auth.url}")
    private String authUrl;

    @Value("${auth.checkurl}")
    private String checkProjurl;

    private RestTemplate client = new RestTemplate();

    Logger log = LoggerFactory.getLogger(AuthClient.class);

    public ResponseEntity<ResponseAuthDto> resolveCookie(RequestAuthDto requestAuthDto) {
        try {
            return client.postForEntity(authUrl, requestAuthDto, ResponseAuthDto.class);
        } catch (Throwable ex) {
            ex.printStackTrace();
            log.error("auth request with error");
        }
        return null;
    }

    public ResponseAuthDto isUserApply(RequestAuthLoginDto requestAuthDto) {
        ResponseEntity<ResponseAuthDto> responseEntity = null;
        try {
            responseEntity = client.postForEntity(authUrl, requestAuthDto, ResponseAuthDto.class);
        } catch (Throwable ex) {
            ex.printStackTrace();
            log.error("auth request with error");
        }
        return responseEntity == null ?
                new ResponseAuthDto("Unknown") : responseEntity.getBody() == null ?
                new ResponseAuthDto("Unknown") : responseEntity.getBody();
    }
}
