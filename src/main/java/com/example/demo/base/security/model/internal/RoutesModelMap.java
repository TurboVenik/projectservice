package com.example.demo.base.security.model.internal;

import java.util.HashSet;

public class RoutesModelMap {

    public static HashSet<KeyDto> routes = new HashSet<KeyDto>() {{

        add(new KeyDto("GET", "projects", ConstantRoles.MANAGER));
        add(new KeyDto("GET", "projects", ConstantRoles.DEVELOPER));

        add(new KeyDto("PUT", "projects", ConstantRoles.MANAGER));
//        add(new KeyDto("PUT", "projects", ConstantRoles.DEVELOPER));

        add(new KeyDto("POST", "projects", ConstantRoles.MANAGER));
//        add(new KeyDto("POST", "projects", ConstantRoles.DEVELOPER));

        add(new KeyDto("DELETE", "projects", ConstantRoles.MANAGER));
//        add(new KeyDto("DELETE", "projects", ConstantRoles.DEVELOPER));


        add(new KeyDto("GET", "tasks", ConstantRoles.MANAGER));
        add(new KeyDto("GET", "tasks", ConstantRoles.DEVELOPER));

        add(new KeyDto("PUT", "tasks", ConstantRoles.MANAGER));
        add(new KeyDto("PUT", "tasks", ConstantRoles.DEVELOPER));

        add(new KeyDto("POST", "tasks", ConstantRoles.MANAGER));
//        add(new KeyDto("POST", "tasks", ConstantRoles.DEVELOPER));

        add(new KeyDto("DELETE", "tasks", ConstantRoles.MANAGER));
//        add(new KeyDto("DELETE", "tasks", ConstantRoles.DEVELOPER));
    }};
}
