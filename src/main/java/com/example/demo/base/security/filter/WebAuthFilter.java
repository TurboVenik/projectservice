package com.example.demo.base.security.filter;

import com.example.demo.base.model.ResponseModel;
import com.example.demo.base.security.AuthClient;
import com.example.demo.base.security.model.external.RequestAuthDto;
import com.example.demo.base.security.model.external.ResponseAuthDto;
import com.example.demo.base.security.model.internal.ConstantRoles;
import com.example.demo.base.security.model.internal.KeyDto;
import com.example.demo.base.security.model.internal.RoutesModelMap;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class WebAuthFilter implements Filter {

    private Logger log = LoggerFactory.getLogger(WebAuthFilter.class);
    private static final String COOKIE = "Cookie";
    private static final String COOKIE2  = "MyCookie";

    @Value("${auth.enabled}")
    private String authEnabled;

    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private AuthClient authClient;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (!"true".equalsIgnoreCase(authEnabled)) {
            chain.doFilter(request, response);
            return;
        }
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String cookie = req.getHeader(COOKIE);
        if (StringUtils.isEmpty(cookie)) {
            cookie = req.getHeader(COOKIE2);
        }

        if (cookie.equalsIgnoreCase(ConstantRoles.ADMIN)) {
            chain.doFilter(request, response);
            return;
        }

        ResponseEntity<ResponseAuthDto> responseAuthDto = null;

        responseAuthDto = authClient.resolveCookie(new RequestAuthDto(cookie, getProjId(req.getServletPath())));

        if (responseAuthDto != null &&
                responseAuthDto.getStatusCode().is2xxSuccessful() &&
                responseAuthDto.getBody() != null &&
                !StringUtils.isEmpty(responseAuthDto.getBody().getRole())) {


            KeyDto requestKey = new KeyDto(
                    req.getMethod(),
                    analyzePath(req.getServletPath()),
                    responseAuthDto.getBody().getRole()
            );
            if (responseAuthDto.getBody().getRole().equalsIgnoreCase(ConstantRoles.ADMIN) || RoutesModelMap.routes.contains(requestKey)) {
                chain.doFilter(request, response);
                return;
            }
        }

        res.setStatus(HttpStatus.FORBIDDEN.value());
        res.setContentType(MediaType.APPLICATION_JSON_VALUE);

        mapper.writeValue(res.getWriter(), ResponseModel.createError("Unauthorized"));
    }


    @Override
    public void destroy() {
    }

    private String getProjId(String path) {
        String[] paths = path.split("/");

        //implement some logic here
        if (paths.length >= 4) {
            return paths[3];
        } else {
            //Project branch
            return null;
        }
    }

    private String analyzePath(String path) {
        String[] paths = path.split("/");

        //implement some logic here
        if (paths.length >= 5) {
            //task branch
            return "tasks";
        } else {
            //Project branch
            return "projects";
        }
    }
}