package com.example.demo.base.security.model.internal;

import java.util.Objects;

public class KeyDto {

    private String method;
    private String path;
    private String role;

    public KeyDto() {
    }

    public KeyDto(String method, String path, String role) {
        this.method = method;
        this.path = path;
        this.role = role;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyDto dto = (KeyDto) o;
        return Objects.equals(method, dto.method) &&
                Objects.equals(path, dto.path) &&
                Objects.equals(role, dto.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(method, path, role);
    }
}
