package com.example.demo.base.security.model.external;

public class ResponseAuthDto {

    private String role;

    public ResponseAuthDto() {
    }

    public ResponseAuthDto(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
