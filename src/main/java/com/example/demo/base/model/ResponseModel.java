package com.example.demo.base.model;

public class ResponseModel<T> {

    private static final String SUCCESS = "success";
    private static final String ERROR = "error";

    private ResponseModel() {
    }

    private ResponseModel(String status, String error, T data) {
        this.status = status;
        this.error = error;
        this.data = data;
    }

    //Can be replaced by enum
    private String status;
    //Can be replace by structure
    private String error;
    private T data;

    public static <T> ResponseModel<T> createSuccess(T data) {
        return new ResponseModel<>(SUCCESS, null, data);
    }

    public static <T> ResponseModel<T> createError(String error) {
        return new ResponseModel<>(ERROR, error, null);
    }


    @Deprecated
    public String getStatus() {
        return status;
    }

    @Deprecated
    public void setStatus(String status) {
        this.status = status;
    }

    @Deprecated
    public String getError() {
        return error;
    }

    @Deprecated
    public void setError(String error) {
        this.error = error;
    }

    @Deprecated
    public T getData() {
        return data;
    }

    @Deprecated
    public void setData(T data) {
        this.data = data;
    }
}
