package com.example.demo.base.configuration;


import com.example.demo.base.security.filter.WebAuthFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebFilterConfiguration {

    @Bean
    public FilterRegistrationBean<WebAuthFilter> loggingFilter(WebAuthFilter webAuthFilter){
        FilterRegistrationBean<WebAuthFilter> registrationBean
                = new FilterRegistrationBean<>();

        registrationBean.setFilter(webAuthFilter);
        registrationBean.addUrlPatterns("/api/*");

        return registrationBean;
    }
}
