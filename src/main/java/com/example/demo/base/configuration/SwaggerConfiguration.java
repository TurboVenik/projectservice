package com.example.demo.base.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        Parameter headerParam = new ParameterBuilder().name("MyCookie").defaultValue("admin").parameterType("header")
                .modelRef(new ModelRef("string")).description("Cookie logined user").required(true).build();

        return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(Arrays.asList(headerParam))
                .groupName("/api")
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(regex("/api.*"))
                .build();
    }

}
