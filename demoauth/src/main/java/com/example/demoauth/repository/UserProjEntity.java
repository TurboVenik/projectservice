package com.example.demoauth.repository;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class UserProjEntity {

    @Id
    private String uuid;
    private String login;
    private String projectId;
    private String role;

    public UserProjEntity() {
    }

    public UserProjEntity(String login, String projectId, String role) {
        this.uuid = UUID.randomUUID().toString();
        this.login = login;
        this.projectId = projectId;
        this.role = role;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
