package com.example.demoauth.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserProjRepository extends CrudRepository<UserProjEntity, String> {

    List<UserProjEntity> findByLoginAndProjectId(String login, String projectId);
}
