package com.example.demoauth.controller.old.registry;

public class RegistryRequestDto {

    private String login;
    private String name;
    private String role;
    private String password;

    @Override
    public String toString() {
        return "RegistryRequestDtoNew{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
