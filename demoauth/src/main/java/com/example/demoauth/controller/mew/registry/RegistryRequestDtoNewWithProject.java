package com.example.demoauth.controller.mew.registry;

public class RegistryRequestDtoNewWithProject extends RegistryRequestDtoNew {

    private String role;
    private String projectId;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
