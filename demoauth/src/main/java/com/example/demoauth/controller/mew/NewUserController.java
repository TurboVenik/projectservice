package com.example.demoauth.controller.mew;

import com.example.demoauth.controller.mew.external.RequestAuthDtoNew;
import com.example.demoauth.controller.mew.external.RequestAuthDtoNewWithProject;
import com.example.demoauth.controller.mew.external.ResponseAuthDtoNew;
import com.example.demoauth.controller.mew.registry.RegistryRequestDtoNew;
import com.example.demoauth.controller.mew.registry.RegistryRequestDtoNewWithProject;
import com.example.demoauth.controller.mew.registry.RegistryResponseDtoNew;
import com.example.demoauth.controller.old.external.RequestAuthDto;
import com.example.demoauth.controller.old.external.ResponseAuthDto;
import com.example.demoauth.controller.old.registry.RegistryRequestDto;
import com.example.demoauth.controller.old.registry.RegistryResponseDto;
import com.example.demoauth.repository.UserAuthEntity;
import com.example.demoauth.repository.UserAuthRepository;
import com.example.demoauth.repository.UserProjEntity;
import com.example.demoauth.repository.UserProjRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class NewUserController implements BaseNewController {


    //login - cookie
    private HashMap<String, String> coockieMap = new HashMap<>();
    //cookie - login
    private HashMap<String, String> revertCoockieMap = new HashMap<>();


    @Autowired
    UserAuthRepository userAuthRepository;
    @Autowired
    UserProjRepository userProjRepository;

    @Value("${admin.cookie}")
    private String adminCoockie;

    @RequestMapping(value = "/checkurl", method = POST)
    public ResponseAuthDtoNew checkUrl(@RequestBody RequestAuthDtoNewWithProject requestAuthDto) {
        System.out.println("...::: resolve coockie :::...");
        System.out.println(requestAuthDto.getLogin());
        System.out.println(requestAuthDto.getProjectId());

        if ("admin".equalsIgnoreCase(requestAuthDto.getLogin())) {
            return new ResponseAuthDtoNew("admin");
        }

        if (StringUtils.isEmpty(requestAuthDto.getLogin()))
            return new ResponseAuthDtoNew("U");

        List<UserProjEntity> userProjEntity = userProjRepository.findByLoginAndProjectId(requestAuthDto.getLogin(), requestAuthDto.getProjectId());

        if (CollectionUtils.isEmpty(userProjEntity)) {
            return new ResponseAuthDtoNew("U");
        }

        return new ResponseAuthDtoNew(userProjEntity.get(0).getRole());
    }

    @RequestMapping(value = "/resolve", method = POST)
    public ResponseAuthDtoNew put(@RequestBody RequestAuthDtoNewWithProject requestAuthDto) {
        System.out.println("...::: resolve coockie :::...");
        System.out.println(requestAuthDto.getCookie());
        System.out.println(requestAuthDto.getProjectId());

        if (adminCoockie.equalsIgnoreCase(requestAuthDto.getCookie())) {
            return new ResponseAuthDtoNew("admin");
        }

        String login = revertCoockieMap.get(requestAuthDto.getCookie());

        if (StringUtils.isEmpty(login))
            return new ResponseAuthDtoNew("Unknown");

        List<UserProjEntity> userProjEntity = userProjRepository.findByLoginAndProjectId(login, requestAuthDto.getProjectId());

        if (CollectionUtils.isEmpty(userProjEntity)) {
            return new ResponseAuthDtoNew("Unknown");
        }

        return new ResponseAuthDtoNew(userProjEntity.get(0).getRole());
    }

    @RequestMapping(value = "/delete-role", method = POST)
    public RegistryResponseDtoNew deleteRole(@RequestBody RegistryRequestDtoNewWithProject projectDto) {
        System.out.println("...::: put Projects :::...");
        System.out.println(projectDto);

        List<UserProjEntity> userProjEntity = userProjRepository.findByLoginAndProjectId(projectDto.getLogin(), projectDto.getProjectId());
        userProjRepository.deleteAll(userProjEntity);

        return new RegistryResponseDtoNew("OK");
    }

    @RequestMapping(value = "/add-role", method = POST)
    public RegistryResponseDtoNew addRole(@RequestBody RegistryRequestDtoNewWithProject projectDto) {
        System.out.println("...::: put Projects :::...");
        System.out.println(projectDto);

        userProjRepository.save(new UserProjEntity(projectDto.getLogin(), projectDto.getProjectId(), projectDto.getRole()));

        return new RegistryResponseDtoNew("OK");
    }

    @RequestMapping(value = "/registry", method = POST)
    public RegistryResponseDtoNew registry(@RequestBody RegistryRequestDtoNew projectDto) {
        System.out.println("...::: put Projects :::...");
        System.out.println(projectDto);

        userAuthRepository.save(new UserAuthEntity(projectDto.getLogin(), projectDto.getPassword(), projectDto.getPassword()));

        return new RegistryResponseDtoNew("OK");
    }

    @RequestMapping(value = "/login", method = POST)
    public RequestAuthDtoNew login(@RequestBody RegistryRequestDtoNew requestAuthDto) {
        System.out.println("...::: generate coockie :::...");
        System.out.println(requestAuthDto);

        String fromMap = coockieMap.get(requestAuthDto.getLogin());
        if (StringUtils.isEmpty(fromMap)) {

            UserAuthEntity userAuthEntity = userAuthRepository.findById(requestAuthDto.getLogin()).orElse(null);

            if (userAuthEntity == null || !userAuthEntity.getPassword().equals(requestAuthDto.getPassword())) {
                return new RequestAuthDtoNew("0");
            } else {
                String uuid = UUID.randomUUID().toString();
                coockieMap.put(requestAuthDto.getLogin(), uuid);
                revertCoockieMap.put(uuid, requestAuthDto.getLogin());
                return new RequestAuthDtoNew(uuid);
            }
        } else {
            return new RequestAuthDtoNew(fromMap);
        }
    }

    @RequestMapping(value = "/logout", method = POST)
    public RequestAuthDtoNew logout(@RequestBody RequestAuthDtoNew requestAuthDto) {
        System.out.println("...::: resolve coockie :::...");
        System.out.println(requestAuthDto.getCookie());

        String login = revertCoockieMap.remove(requestAuthDto.getCookie());
        coockieMap.remove(login);

        return new RequestAuthDtoNew("0");
    }

}
