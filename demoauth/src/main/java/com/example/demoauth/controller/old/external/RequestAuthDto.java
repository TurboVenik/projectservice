package com.example.demoauth.controller.old.external;

public class RequestAuthDto {

    private String cookie;

    public RequestAuthDto() {
    }

    public RequestAuthDto(String cookie) {
        this.cookie = cookie;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }
}
