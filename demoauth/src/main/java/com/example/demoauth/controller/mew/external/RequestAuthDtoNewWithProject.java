package com.example.demoauth.controller.mew.external;

public class RequestAuthDtoNewWithProject extends RequestAuthDtoNew {

    private String login;
    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
