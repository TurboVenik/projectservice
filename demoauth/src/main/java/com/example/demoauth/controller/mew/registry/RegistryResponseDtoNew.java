package com.example.demoauth.controller.mew.registry;

public class RegistryResponseDtoNew {

    private String status;

    public RegistryResponseDtoNew(String status) {
        this.status = status;
    }

    public RegistryResponseDtoNew() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
