package com.example.demoauth.controller.mew.external;

public class RequestAuthDtoNew {

    private String cookie;

    public RequestAuthDtoNew() {
    }

    public RequestAuthDtoNew(String cookie) {
        this.cookie = cookie;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }
}
