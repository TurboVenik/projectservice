package com.example.demoauth.controller.old;

import com.example.demoauth.controller.old.external.RequestAuthDto;
import com.example.demoauth.controller.old.external.ResponseAuthDto;
import com.example.demoauth.controller.old.registry.RegistryRequestDto;
import com.example.demoauth.controller.old.registry.RegistryResponseDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

//Оставил Славе
@RestController
public class UserController {

    //Coockie - role
    private HashMap<String, String> coockieMap = new HashMap<>();

    private HashMap<String, RegistryRequestDto> loginMap = new HashMap<>();

    @RequestMapping(value = "/coockie", method = POST)
    public RequestAuthDto getCoockie(@RequestBody RegistryRequestDto requestAuthDto) {
        System.out.println("...::: resolve coockie :::...");
        System.out.println(requestAuthDto);

        RegistryRequestDto fromMap = loginMap.get(requestAuthDto.getLogin());
        if (fromMap == null || fromMap.getPassword() == null || !fromMap.getPassword().equals(requestAuthDto.getPassword()))
            return new RequestAuthDto("0");

        String uuid = UUID.randomUUID().toString();

        coockieMap.put(uuid, fromMap.getRole());

        return new RequestAuthDto(uuid);
    }

    @RequestMapping(value = "/auth", method = POST)
    public ResponseAuthDto put(@RequestBody RequestAuthDto requestAuthDto) {
        System.out.println("...::: resolve coockie :::...");
        System.out.println(requestAuthDto.getCookie());

        String role = coockieMap.get(requestAuthDto.getCookie());
        if (role == null) {
            return new ResponseAuthDto("unknown");
        }

        return new ResponseAuthDto(role);
    }

    @RequestMapping(value = "/registry", method = POST)
    public RegistryResponseDto registry(@RequestBody RegistryRequestDto projectDto) {
        System.out.println("...::: put Projects :::...");
        System.out.println(projectDto);
        loginMap.put(projectDto.getLogin(), projectDto);

        return new RegistryResponseDto("OK");
    }

}
