package com.example.demoauth.controller.old.external;

public class ResponseAuthDto {

    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ResponseAuthDto() {
    }

    public ResponseAuthDto(String role) {
        this.role = role;
    }
}
