package com.example.demoauth.controller.old.registry;

public class RegistryResponseDto {

    private String status;

    public RegistryResponseDto(String status) {
        this.status = status;
    }

    public RegistryResponseDto() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
