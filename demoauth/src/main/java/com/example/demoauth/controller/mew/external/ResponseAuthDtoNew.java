package com.example.demoauth.controller.mew.external;

public class ResponseAuthDtoNew {

    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ResponseAuthDtoNew() {
    }

    public ResponseAuthDtoNew(String role) {
        this.role = role;
    }
}
